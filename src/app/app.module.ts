import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, FormControlDirective} from '@angular/forms';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {SearchedResultListComponent} from './searched-result-list/searched-result-list.component';
import {GitHubAPIServiceService} from "./services/git-hub-apiservice.service";
import {HttpClientModule} from "@angular/common/http";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
    imports: [BrowserModule, FormsModule, HttpClientModule, NgxPaginationModule],
    declarations: [AppComponent, HeaderComponent, SearchedResultListComponent, FormControlDirective],
    bootstrap: [AppComponent],
    providers: [GitHubAPIServiceService]
})
export class AppModule {
}
