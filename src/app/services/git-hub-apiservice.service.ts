import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError, BehaviorSubject} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class GitHubAPIServiceService {

  constructor(private http: HttpClient) {}

  searchSubject = new BehaviorSubject('');
  sortingSubject = new BehaviorSubject('');
  private baseUrl: string = 'https://api.github.com/';

  getHeader () {
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');
    return headers;
  }

  setSearchText(queryText) {
    this.searchSubject.next(queryText);
  }

  setSorting(queryText) {
    this.sortingSubject.next(queryText);
  }

  getSearchResults(searchQuery, page, limit): Observable<any> {
    const url = this.baseUrl + 'search/users?q=' + searchQuery + '&page=' + page + '&per_page=' + limit;
    return this.http.get(url, {headers:  this.getHeader()})
        .pipe(
            catchError(this.handleError)
        );
  }

  getUserRepos(username): Observable<any> {
    const url = this.baseUrl + 'users/' + username + '/repos';
    return this.http.get(url, {headers:  this.getHeader()})
        .pipe(
            catchError(this.handleError)
        );
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
