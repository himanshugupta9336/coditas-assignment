import { TestBed } from '@angular/core/testing';

import { GitHubAPIServiceService } from './git-hub-apiservice.service';

describe('GitHubAPIServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GitHubAPIServiceService = TestBed.get(GitHubAPIServiceService);
    expect(service).toBeTruthy();
  });
});
