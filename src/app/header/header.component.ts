import { Component, OnInit } from '@angular/core';
import {GitHubAPIServiceService} from "../services/git-hub-apiservice.service";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {Constants} from "../constants";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [Constants]
})
export class HeaderComponent implements OnInit {

  constructor(private githubService: GitHubAPIServiceService, public constants: Constants) { }
  searchListControl: FormControl = new FormControl('');
  sortingControl: FormControl = new FormControl('Select Sorting Order');

  ngOnInit() {
    this.searchControllerText();
    this.rankText();
  }

  searchControllerText() {
    this.searchListControl.valueChanges.pipe(debounceTime(1000)).pipe(distinctUntilChanged()).subscribe(
        query => {
          this.githubService.setSearchText(query);
    });
  }

  rankText() {
    this.sortingControl.valueChanges.pipe(distinctUntilChanged()).subscribe(
        sortingText => {
          this.githubService.setSorting(sortingText);
        });
  }

}
