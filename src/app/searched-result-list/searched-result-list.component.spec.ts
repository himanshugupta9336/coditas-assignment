import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchedResultListComponent } from './searched-result-list.component';

describe('SearchedResultListComponent', () => {
  let component: SearchedResultListComponent;
  let fixture: ComponentFixture<SearchedResultListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchedResultListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchedResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
