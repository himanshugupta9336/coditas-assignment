import { Component, OnInit } from '@angular/core';
import {GitHubAPIServiceService} from "../services/git-hub-apiservice.service";
import {Constants} from "../constants";


@Component({
  selector: 'app-searched-result-list',
  templateUrl: './searched-result-list.component.html',
  styleUrls: ['./searched-result-list.component.css'],
  providers: [Constants]
})
export class SearchedResultListComponent implements OnInit {

  constructor(private githubService: GitHubAPIServiceService, private constants: Constants) { }

  userList;
  totalCount;
  page: number = 1;
  limit = 10;

  ngOnInit() {
    this.getUserList();
    this.getCurrentSorting();
  }

  getUserList(isPagination: boolean = false) {
    this.githubService.searchSubject.subscribe(
        searchQuery => {
          if (!isPagination) {
            this.page = 1;
          }
          if (searchQuery && searchQuery.trim()) {
            this.githubService.getSearchResults(searchQuery, this.page, this.limit).subscribe(
                data => this.userListDataSuccess(data)
            )
          }
        }
    );
  }

  userListDataSuccess(data) {
    this.totalCount = data.total_count;
    this.userList = data.items;
  }

  getRepoOfSingleUser(userData, isCollapse) {
    userData.isCollapse = isCollapse;
    if (isCollapse) {
      this.githubService.getUserRepos(userData.login).subscribe(
          data => userData.repoList = data
      );
    }
  }

  getCurrentSorting() {
    this.githubService.sortingSubject.subscribe(
        sortBy => this.arrangeSortingOrderOfUserList(sortBy)
    );
  }

  arrangeSortingOrderOfUserList(sortBy) {
    if(this.userList && this.userList.length > 0) {
      this.userList.sort((a, b) => {
        switch (sortBy) {
          case this.constants.NAME_ASC: {
            return  (a.login.toLowerCase() > b.login.toLowerCase()) ? 1 : ((b.login.toLowerCase() > a.login.toLowerCase()) ? -1 : 0)
          }
          case this.constants.NAME_DESC: {
            return  (b.login.toLowerCase() > a.login.toLowerCase()) ? 1 : ((a.login.toLowerCase() > b.login.toLowerCase()) ? -1 : 0)
          }
          case this.constants.RANK_ASC: {
            return parseFloat(a.score) - parseFloat(b.score);
          }
          case this.constants.RANK_DESC: {
            return parseFloat(b.score) - parseFloat(a.score);
          }
        }
      });
    }
  }

  onPageChange (event) {
    this.page = event;
    this.getUserList(true);
  }

}
